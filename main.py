import os
import argparse
import tensorflow as tf
from models.DAE import DAE
from models.VAE import VAE
from models.beta_VAE import beta_VAE
from models.ALI import ALI


def parse_args():
	desc = 'Tensorflow implementation of autoencoder-based model collections'

	parser = argparse.ArgumentParser(description = desc)
	parser.add_argument('--model', type = str, default = 'VAE', choices = ['DAE', 'VAE', 'beta_VAE', 'ALI'], 
						help = 'The type of autoencoder models')
	parser.add_argument('--dataset', type = str, default  = 'mnist', choices = ['mnist', 'fashion_mnist', 'celeb_a', 'cifar10'],
						help = 'The name of dataset')
	parser.add_argument('--epoch', type = int, default = 20, help = 'The number of training epochs')
	parser.add_argument('--batch_size', type = int, default = 64, help = 'The size of batch')
	parser.add_argument('--learning_rate', type = float, default = 0.0002, help = 'Learning rate')
	parser.add_argument('--z_dim', type = int, default = 10, help = 'The size of the latent variables')

	return parser.parse_args()

def main():

	args = parse_args()
	if args is None:
		exit()

	models = [DAE, VAE, beta_VAE, ALI]

	with tf.Session() as sess:

		ae = None
		for model in models:
			if args.model == model.model_name:
				ae = model(sess, epoch = args.epoch, 
					batch_size = args.batch_size, learning_rate = args.learning_rate, 
					z_dim = args.z_dim, dataset = args.dataset)
		if ae is None:
			raise Exception('There is no option for ' + args.model)

		ae.build_model()

		print('training start!')

		ae.train()

		print('training finished!')

if __name__ == '__main__':
	main()