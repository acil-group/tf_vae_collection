import os
import numpy as np
import scipy.misc
from scipy.misc import imresize
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_datasets as tfds
from sklearn.metrics import normalized_mutual_info_score, adjusted_rand_score

def data_loader(dataset_name, data_path=None):
	"""
	:param dataset_name: 29 datasets available, this code only consider
						 'mnist', 'fashion_mnist', 'celeb_a', 'cifar10'
	:param data_path: the directory to keep the downloaded dataset, defalut to "~/tensorflow_datasets"
	:return x_train, y_train, x_test, y_test
	"""

	# downloading dataset
	dataset = tfds.load(dataset_name, data_dir=data_path, batch_size = -1)

	train_data, test_data = tfds.as_numpy(dataset['train']), tfds.as_numpy(dataset['test'])
	x_train, y_train = train_data['image'], train_data['label']
	x_test, y_test = test_data['image'], test_data['label']

	# normalizing
	x_train, x_test = x_train / 255.0, x_test / 255.0 

	return x_train, y_train, x_test, y_test


def save_images(images, path):

	num_samples, h, w, c = images.shape[0], images.shape[1], images.shape[2], images.shape[3]

	frame_dim = int(np.sqrt(num_samples))
	canvas = np.empty((h * frame_dim, w * frame_dim))
	for idx, image in enumerate(images):
		i = idx // frame_dim
		j = idx % frame_dim
		canvas[i*h : (i+1)*h, j*w : (j+1)*w] = np.squeeze(image)

	scipy.misc.imsave(path, canvas)

##### evaluation metrics #####
def acc(Y_true, Y_pred):

	from sklearn.utils.linear_assignment_ import linear_assignment
	assert Y_pred.size == Y_true.size
	D = max(Y_pred.max(), Y_true.max())+1
	w = np.zeros((D,D), dtype=np.int64)
	for i in range(Y_pred.size):
		w[Y_pred[i], Y_true[i]] += 1
	ind = linear_assignment(w.max() - w)

	return sum([w[i,j] for i,j in ind])*1.0/Y_pred.size

def nmi(Y_true, Y_pred):
	return normalized_mutual_info_score(Y_true, Y_pred)

def ari(Y_true, Y_pred):
	return adjusted_rand_score(Y_true, Y_pred)
##############################