import os, csv
import time
import numpy as np
import tensorflow as tf
from sklearn.manifold import TSNE
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from utils import *

class VAE(object):

	model_name = 'VAE'
	dataset_dir = 'dir_dataset' # directory to download datasets
	checkpoint_dir = 'dir_checkpoint' # directory to save checkpoints
	result_dir = 'dir_result' # directory to save experimental results
	log_dir = 'dir_log' # directory to save logs for tensorboard visualization

	def __init__(self, sess, epoch, batch_size, learning_rate, z_dim, dataset):
		self.sess = sess
		self.epoch = epoch # total training epochs
		self.batch_size = batch_size
		self.learning_rate = learning_rate
		self.z_dim = z_dim  # dimension of latent variables
		self.dataset = dataset # the name of dataset
		self.x_train, self.y_train, self.x_test, self.y_test = data_loader(dataset) # loading dataset
		self.image_dim = [32, 32, 3]

		if dataset == 'mnist' or dataset == 'fashion_mnist':
			self.x_train = tf.image.resize_images(self.x_train, (32, 32)).eval()
			self.x_test = tf.image.resize_images(self.x_test, (32, 32)).eval()
			self.image_dim = [32, 32, 1]

		self.num_batch = len(self.x_train) // self.batch_size

	def encoder(self, x, is_training=True, reuse=False):

		with tf.variable_scope('encoder', reuse=reuse):

			# 1st convolutional layer
			conv1 = tf.layers.conv2d(inputs=x, filters=64, kernel_size=[4, 4], strides=(2, 2), padding='same')
			lr_conv1 = tf.nn.leaky_relu(conv1)

			#2nd convolutional layer
			conv2 = tf.layers.conv2d(inputs=lr_conv1, filters=128, kernel_size=[4, 4], strides=(2, 2), padding='same')
			lr_conv2 = tf.nn.leaky_relu(tf.layers.batch_normalization(conv2, training = is_training))

			# flatten the convolutional layer
			flat = tf.layers.flatten(lr_conv2)
			#flat = tf.reshape(lr_conv2, [self.batch_size, -1])

			# 1st fully-connected layer
			fc1 = tf.layers.dense(flat, units=1024)
			lr_fc1 = tf.nn.leaky_relu(tf.layers.batch_normalization(fc1, training = is_training))

			# output layer
			out = tf.layers.dense(lr_fc1, units=2*self.z_dim)
			# The mean parameter is unconstrained
			mean = out[:, :self.z_dim]
			# The standard deviation must be positive. Parametrize with a softplus and
			# add a small epsilon for numerical stability
			stddev = 1e-6 + tf.nn.softplus(out[:, self.z_dim:])

			return mean, stddev

	def decoder(self, z, is_training=True, reuse=False):

		with tf.variable_scope('decoder', reuse=reuse):

			# 1st fully-connected layer
			fc1 = tf.layers.dense(z, units=1024)
			r_fc1 = tf.nn.relu(tf.layers.batch_normalization(fc1, training=is_training))

			# 2nd fully-connected layer
			fc2 = tf.layers.dense(r_fc1, units=8 * 8 * 128)
			r_fc2 = tf.nn.relu(tf.layers.batch_normalization(fc2, training=is_training))

			# reshape the fully-connected layer
			deflat = tf.reshape(r_fc2, [-1, 8, 8, 128])

			# 1st deconvolutional layer
			deconv1 = tf.layers.conv2d_transpose(inputs=deflat, filters=64, kernel_size=[4, 4], strides=(2, 2), padding='same')
			r_deconv1 = tf.nn.relu(tf.layers.batch_normalization(deconv1, training=is_training))

			# output layer
			deconv_out = tf.layers.conv2d_transpose(inputs=r_deconv1, filters=1, kernel_size=[4, 4], strides=(2, 2), padding='same')
			out = tf.nn.sigmoid(deconv_out)

			return out

	def build_model(self):

		# placeholder
		self.input_x = tf.placeholder(dtype = tf.float32, shape = [None] + self.image_dim, name = 'input_x')
		self.input_z = tf.placeholder(dtype = tf.float32, shape = [None, self.z_dim], name = 'input_z')

		# encoding 
		mu, sigma = self.encoder(self.input_x, is_training = True, reuse = False)
		z = mu + sigma * tf.random_normal(tf.shape(mu), 0, 1, dtype = tf.float32)

		# decoding 
		outputs = self.decoder(z, is_training = True, reuse = False)
		self.outputs = tf.clip_by_value(outputs, 1e-8, 1 - 1e-8)

		# loss 
		self.nll = -tf.reduce_mean(tf.reduce_sum(self.input_x * tf.log(self.outputs) 
			+ (1 - self.input_x) * tf.log(1 - self.outputs), [1, 2]))
		self.KL_div = tf.reduce_mean(0.5 * tf.reduce_sum(tf.square(mu) + tf.square(sigma) 
			- tf.log(1e-8 + tf.square(sigma)) - 1, [1]))
		ELBO = -(self.nll + self.KL_div)
		self.loss = -ELBO

		# optimizer
		t_vars = tf.trainable_variables()
		with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
			self.optim = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss, var_list = t_vars)

		# testing 
		self.generated_images = self.decoder(self.input_z, is_training=False, reuse=True)
		self.encode_latent = z

		# summary 
		tf.summary.scalar('KL', self.KL_div)
		tf.summary.scalar('NLL', self.nll)
		tf.summary.scalar('loss', self.loss)
		self.summary_op = tf.summary.merge_all()

	def train(self):

		# initialize variables
		self.sess.run(tf.global_variables_initializer())

		# keep 5 latest checkpoints
		self.saver = tf.train.Saver(max_to_keep=5) 

		# tensorboard
		self.writer = tf.summary.FileWriter(self.log_dir + '/' + self.model_dir, self.sess.graph)

		# restore trained model if exists
		counter = self.load_model(self.checkpoint_dir)

		if counter:
			start_epoch = int(counter/self.num_batch)
			print('The model has been trained for ', start_epoch, ' epochs')
			global_step = counter
		else:
			start_epoch = 0
			global_step = 1

		# write results to csv file
		self.dir = self.make_dirs(os.path.join(self.result_dir, self.model_dir))
		f = open(self.dir + '/VAEC.csv', 'w', newline='')
		self.logwriter = csv.DictWriter(f, fieldnames=['epoch', 'k_acc', 'k_nmi', 'k_ari', 'g_acc', 'g_nmi', 'g_ari'])
		self.logwriter.writeheader()

		# start training
		start_time = time.time()

		for epoch in range(start_epoch, self.epoch):
			for idx in range(self.num_batch):

				batch_x = self.x_train[idx*self.batch_size : (idx+1)*self.batch_size]
				batch_z = np.random.normal(0, 1, (self.batch_size, self.z_dim))

				_, loss, summary = self.sess.run([self.optim, self.loss, self.summary_op], feed_dict = {self.input_x: batch_x})
				self.writer.add_summary(summary, global_step)
				global_step += 1

				if idx % 100 == 0:
					print('epoch:{}, iteration:{}, time:{}, loss:{}'.format(epoch, idx, time.time() - start_time, loss))

			# save model every epoch
			self.save_model(self.checkpoint_dir, global_step)

			# conduct experiment every epoch
			self.experiments(epoch)

	def experiments(self, epoch):

		# directory to save experimental results
		dir = self.make_dirs(os.path.join(self.result_dir, self.model_dir))

		"""
		first experiment: generate images
		"""

		# the number of samples
		nx = ny = 10
		num_samples = nx * ny

		# sampling z from N(0, 1) prior
		z_sample = np.random.normal(0, 1, (num_samples, self.z_dim))

		# generate images
		samples = self.sess.run(self.generated_images, feed_dict={self.input_z: z_sample})

		# save images
		save_images(samples, dir + '/images_epoch_{}'.format(epoch) + '.png')


		"""
		second experiment: t-sne visualization of latent space
		"""		
		latent = self.sess.run(self.encode_latent, feed_dict={self.input_x: self.x_test})
		labels = self.y_test
		visualize_tsne = TSNE(random_state = 123).fit_transform(latent)
		
		plt.figure(figsize=(10, 10))
		colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'gray', 'orange', 'purple']
		digits = range(10)
		for i, c in zip(digits, colors):
		    plt.scatter(visualize_tsne[labels == i, 0], visualize_tsne[labels == i, 1], c = c, label = i)
		plt.legend()
		plt.xticks([])
		plt.yticks([])
		plt.savefig(dir + '/latent_epoch_{}'.format(epoch) + '.png')

		"""
		third experiment: clustering on latent space
		"""		
	
		# k means clustering on latent representations
		kmeans = KMeans(n_clusters=10, n_init=10)
		kmeans.fit(latent)
		y_pred = kmeans.predict(latent)
		k_acc = acc(self.y_test, y_pred)
		k_nmi = nmi(self.y_test, y_pred)
		k_ari = ari(self.y_test, y_pred)
		print('kmeans on feature space: epoch:{}, acc:{}, nmi:{}, ari:{}'.format(epoch, k_acc, k_nmi, k_ari))

		# Gaussian Mixture clustering on latent representations
		gmm = GaussianMixture(n_components=10, n_init=10)
		gmm.fit(latent)
		y_pred = gmm.predict(latent)
		g_acc = acc(self.y_test, y_pred)
		g_nmi = nmi(self.y_test, y_pred)
		g_ari = ari(self.y_test, y_pred)
		print('gmm on feature space: epoch:{}, acc:{}, nmi:{}, ari:{}'.format(epoch, g_acc, g_nmi, g_ari))

		# writing results to csv file
		logdict = dict(epoch=epoch, k_acc=k_acc, k_nmi=k_nmi, k_ari=k_ari, g_acc=g_acc, g_nmi=g_nmi, g_ari=g_ari)
		self.logwriter.writerow(logdict)

		"""
		third experiment: visualize data manifold(only ppossible when latent dimension is 2)
		"""
		if self.z_dim == 2:

			nx = ny = 20
			z_range = 4

			# sampling z
			z_sample = np.rollaxis(np.mgrid[z_range:-z_range:ny * 1j, z_range:-z_range:nx * 1j], 0, 3)
			z_sample = np.reshape(z_sample, [-1, 2])

			# generate images
			samples = self.sess.run(self.generated_images, feed_dict={self.input_z: z_sample})

			# save manifold
			save_images(samples, dir + '/manifold_epoch_{}'.format(epoch) + '.png')

			# plot scatter image of test data manifold
			# borrowed from https://github.com/hwalsuklee/tensorflow-generative-model-collections/blob/master/VAE.py

			latent = self.sess.run(self.encode_latent, feed_dict={self.input_x: self.x_test})
			labels = self.y_test

			# plot scatter image of test data manifold
			colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'gray', 'orange', 'purple']
			digits = range(10)
			plt.figure(figsize=(10, 10))

			for i, c in zip(digits, colors):
				plt.scatter(latent[labels == i, 0], latent[labels == i, 1], c = c, label = i)

			plt.legend()
			plt.savefig(dir + '/scatter_epoch_{}'.format(epoch) + '.png')
			

	@property
	def model_dir(self):
		return '{}_{}_{}'.format(self.model_name, self.dataset, self.z_dim)

	def make_dirs(self, dir):
		if not os.path.exists(dir):
			os.makedirs(dir)
		return dir

	def save_model(self, checkpoint_dir, step):
		dir = self.make_dirs(os.path.join(checkpoint_dir, self.model_dir))
		self.saver.save(self.sess, dir + '/ckpt', global_step = step)

	def load_model(self, checkpoint_dir):
		import re
		checkpoint_dir = os.path.join(checkpoint_dir, self.model_dir)
		ckpt = tf.train.get_checkpoint_state(checkpoint_dir)

		if ckpt and ckpt.model_checkpoint_path:
			ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
			self.saver.restore(self.sess, os.path.join(checkpoint_dir, ckpt_name))
			counter = int(next(re.finditer("(\d+)(?!.*\d)",ckpt_name)).group(0))
			print('Successful to find a checkpoint!')
			return counter
		else:
			print('Failed to find a checkpoint!')
			return 0
