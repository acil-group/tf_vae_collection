import os
import time
import numpy as np
import tensorflow as tf
from sklearn.manifold import TSNE
from utils import *

class ALI(object):

	model_name = 'ALI'
	dataset_dir = 'dir_dataset' # directory to download datasets
	checkpoint_dir = 'dir_checkpoint' # directory to save checkpoints
	result_dir = 'dir_result' # directory to save experimental results
	log_dir = 'dir_log' # directory to save logs for tensorboard visualization

	def __init__(self, sess, epoch, batch_size, learning_rate, z_dim, dataset):
		self.sess = sess
		self.epoch = epoch # total training epochs
		self.batch_size = batch_size
		self.learning_rate = learning_rate
		self.z_dim = z_dim  # dimension of latent variables
		self.dataset = dataset # the name of dataset
		self.x_train, self.y_train, self.x_test, self.y_test = data_loader(dataset) # loading dataset
		self.image_dim = [32, 32, 3]

		if dataset == 'mnist' or dataset == 'fashion_mnist':
			self.x_train = tf.image.resize_images(self.x_train, (32, 32)).eval()
			self.x_test = tf.image.resize_images(self.x_test, (32, 32)).eval()
			self.image_dim = [32, 32, 1]

		self.num_batch = len(self.x_train) // self.batch_size

	def encoder(self, x, is_training=True, reuse=False):

		with tf.variable_scope('encoder', reuse=reuse):

			# 1st convolutional layer
			conv1 = tf.layers.conv2d(inputs=x, filters=64, kernel_size=[4, 4], strides=(2, 2), padding='same')
			lr_conv1 = tf.nn.leaky_relu(conv1)

			#2nd convolutional layer
			conv2 = tf.layers.conv2d(inputs=lr_conv1, filters=128, kernel_size=[4, 4], strides=(2, 2), padding='same')
			lr_conv2 = tf.nn.leaky_relu(tf.layers.batch_normalization(conv2, training = is_training))

			# flatten the convolutional layer
			flat = tf.reshape(lr_conv2, [self.batch_size, -1])

			# 1st fully-connected layer
			fc1 = tf.layers.dense(flat, units=1024)
			lr_fc1 = tf.nn.leaky_relu(tf.layers.batch_normalization(fc1, training = is_training))

			# output layer
			out = tf.layers.dense(lr_fc1, units=self.z_dim)

			return out

	def decoder(self, z, is_training=True, reuse=False):

		with tf.variable_scope('decoder', reuse=reuse):

			# 1st fully-connected layer
			fc1 = tf.layers.dense(z, units=1024)
			r_fc1 = tf.nn.relu(tf.layers.batch_normalization(fc1, training=is_training))

			# 2nd fully-connected layer
			fc2 = tf.layers.dense(r_fc1, units=8 * 8 * 128)
			r_fc2 = tf.nn.relu(tf.layers.batch_normalization(fc2, training=is_training))

			# reshape the fully-connected layer
			deflat = tf.reshape(r_fc2, [-1, 8, 8, 128])

			# 1st deconvolutional layer
			deconv1 = tf.layers.conv2d_transpose(inputs=deflat, filters=64, kernel_size=[4, 4], strides=(2, 2), padding='same')
			r_deconv1 = tf.nn.relu(tf.layers.batch_normalization(deconv1, training=is_training))

			# output layer
			deconv_out = tf.layers.conv2d_transpose(inputs=r_deconv1, filters=1, kernel_size=[4, 4], strides=(2, 2), padding='same')
			out = tf.nn.tanh(deconv_out)

			return out

	def discriminator(self, x, z, is_training=True, reuse=False):

		with tf.variable_scope('discriminator', reuse=reuse):

			# 1st convolutional layer
			conv1 = tf.layers.conv2d(inputs=x, filters=64, kernel_size=[4, 4], strides=(2, 2), padding='same')
			lr_conv1 = tf.nn.leaky_relu(conv1)

			#2nd convolutional layer
			conv2 = tf.layers.conv2d(inputs=lr_conv1, filters=128, kernel_size=[4, 4], strides=(2, 2), padding='same')
			lr_conv2 = tf.nn.leaky_relu(tf.layers.batch_normalization(conv2, training=is_training))

			# flatten the convolutional layer
			flat = tf.reshape(lr_conv2, [self.batch_size, -1])
			x_z = tf.concat((flat, z), 1)

			# 1st fully-connected layer
			fc1 = tf.layers.dense(x_z, units=1024)
			lr_fc1 = tf.nn.leaky_relu(tf.layers.batch_normalization(fc1, training=is_training))

			# output layer
			logits = tf.layers.dense(lr_fc1, units=1)
			out = tf.nn.sigmoid(logits)

			return logits, out

	def build_model(self):

		# placeholder
		self.input_x = tf.placeholder(dtype=tf.float32, shape=[self.batch_size] + self.image_dim, name='input_x')
		self.input_z = tf.placeholder(dtype=tf.float32, shape=[self.batch_size, self.z_dim], name='input_z')

		# encoding 
		self.G_x = self.decoder(self.input_z, is_training=True, reuse=False)

		# decoding
		self.G_z = self.encoder(self.input_x, is_training=True, reuse=False)

		# discriminating
		self.G_x_logits, self.D_G_x = self.discriminator(self.G_x, self.input_z, is_training=True, reuse=False)
		self.G_z_logits, self.D_G_z = self.discriminator(self.input_x, self.G_z, is_training=True, reuse=True)

		# loss 
		self.d_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.G_x_logits, labels=tf.zeros_like(self.G_x_logits))
									+ tf.nn.sigmoid_cross_entropy_with_logits(logits=self.G_z_logits, labels=tf.ones_like(self.G_z_logits)))
		self.g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.G_x_logits, labels=tf.ones_like(self.G_x_logits))
									+ tf.nn.sigmoid_cross_entropy_with_logits(logits=self.G_z_logits, labels=tf.zeros_like(self.G_z_logits)))
		
		# optimizer
		with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):

			enc_vars = [var for var in tf.trainable_variables() if 'encoder' in var.name]
			dec_vars = [var for var in tf.trainable_variables() if 'decoder' in var.name]
			d_vars = [var for var in tf.trainable_variables() if 'discriminator' in var.name]

			self.d_optim = tf.train.AdamOptimizer(self.learning_rate, beta1=0.5).minimize(self.d_loss, var_list=d_vars)
			self.g_optim = tf.train.AdamOptimizer(self.learning_rate, beta1=0.5).minimize(self.g_loss, var_list=enc_vars+dec_vars)

		# testing 
		self.generated_images = self.decoder(self.input_z, is_training=False, reuse=True)
		self.encode_latent = self.encoder(self.input_x, is_training=False, reuse=True)

		# summary 
		tf.summary.scalar('d_loss', self.d_loss)
		tf.summary.scalar('g_loss', self.g_loss)
		self.summary_op = tf.summary.merge_all()

	def train(self):

		# initialize variables
		self.sess.run(tf.global_variables_initializer())

		# keep 5 latest checkpoints
		self.saver = tf.train.Saver(max_to_keep=5) 

		# tensorboard
		self.writer = tf.summary.FileWriter(self.log_dir + '/' + self.model_dir, self.sess.graph)

		# restore trained model if exists
		counter = self.load_model(self.checkpoint_dir)

		if counter:
			start_epoch = int(counter/self.num_batch)
			print('The model has been trained for ', start_epoch, ' epochs')
			global_step = counter
		else:
			start_epoch = 0
			global_step = 1

		# write csv file
		self.dir = self.make_dirs(os.path.join(self.result_dir, self.model_dir))
		f = open(self.dir + '/ALIC.csv', 'w', newline='')
		self.logwriter = csv.DictWriter(f, fieldnames=['epoch', 'k_acc', 'g_acc'])
		self.logwriter.writeheader()

		# start training
		start_time = time.time()
		for epoch in range(start_epoch, self.epoch):
			for idx in range(self.num_batch):
				
				batch_x = self.x_train[idx*self.batch_size : (idx+1)*self.batch_size]
				batch_z = np.random.normal(0, 1, (self.batch_size, self.z_dim))

				d_, g_, d_loss, g_loss, summary = self.sess.run([self.d_optim, self.g_optim, self.d_loss, self.g_loss, self.summary_op], 
					feed_dict = {self.input_x: batch_x, self.input_z: batch_z})
				self.writer.add_summary(summary, global_step)
				global_step += 1

				if idx % 100 == 0:
					print('epoch:{}, iteration:{}, time:{}, d_loss:{}, g_loss:{}'.format(epoch, idx, time.time() - start_time, d_loss, g_loss))

			self.save_model(self.checkpoint_dir, global_step)

			# conduct experiment at every epoch
			self.experiments(epoch)			


	def experiments(self, epoch):

		# directory to save experimental results
		dir = self.make_dirs(os.path.join(self.result_dir, self.model_dir))

		"""
		first experiment: generate images
		"""

		# the number of samples
		num_samples = self.batch_size

		# sampling z from N(0, 1) prior
		z_sample = np.random.normal(0, 1, (num_samples, self.z_dim))

		# generate images
		samples = self.sess.run(self.generated_images, feed_dict={self.input_z: z_sample})

		# save images
		save_images(samples, dir + '/images_epoch_{}'.format(epoch) + '.png')

		"""
		second experiment: visualize data manifold
		"""
		if self.z_dim == 2:

			nx = ny = 8
			z_range = 4

			# sampling z
			z_sample = np.rollaxis(np.mgrid[z_range:-z_range:ny * 1j, z_range:-z_range:nx * 1j], 0, 3)
			z_sample = np.reshape(z_sample, [-1, 2])

			# generate images
			samples = self.sess.run(self.generated_images, feed_dict={self.input_z: z_sample})

			# save manifold
			save_images(samples, dir + '/manifold_epoch_{}'.format(epoch) + '.png')

			# plot scatter image of test data manifold
			# borrowed from https://github.com/hwalsuklee/tensorflow-generative-model-collections/blob/master/VAE.py
			latent_tot, labels_tot = None, None
			for i in range(len(self.x_test) // self.batch_size):
				latent = self.sess.run(self.encode_latent, feed_dict={self.input_x: self.x_test[i*self.batch_size: (i+1)*self.batch_size]})
				labels = self.y_test[i*self.batch_size: (i+1)*self.batch_size]
				if i == 0:
					latent_tot = latent
					labels_tot = labels
				else:
					latent_tot = np.concatenate((latent_tot, latent), axis=0)
					labels_tot = np.concatenate((labels_tot, labels), axis=0)

			# plot scatter image of test data manifold
			colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'gray', 'orange', 'purple']
			digits = range(10)
			plt.figure(figsize=(8, 6))

			for i, c in zip(digits, colors):
				plt.scatter(latent_tot[labels_tot == i, 0], latent_tot[labels_tot == i, 1], c = c, label = i)

			plt.legend()
			plt.savefig(dir + '/scatter_epoch_{}'.format(epoch) + '.png')

		"""
		third experiment: visualize latent space
		"""

		# latent representations and clustering
		latent_tot, labels_tot = None, None
		for i in range(len(self.x_test) // self.batch_size):
			latent = self.sess.run(self.encode_latent, feed_dict={self.input_x: self.x_test[i*self.batch_size: (i+1)*self.batch_size]})
			labels = self.y_test[i*self.batch_size: (i+1)*self.batch_size]
			if i == 0:
				latent_tot = latent
				labels_tot = labels
			else:
				latent_tot = np.concatenate((latent_tot, latent), axis=0)
				labels_tot = np.concatenate((labels_tot, labels), axis=0)

		#z = self.sess.run(self.encode_latent, feed_dict={self.input_x: self.x_test})

		# tsne visualization of latent representations
		visualize_tsne = TSNE(random_state = 123).fit_transform(latent_tot)
		plt.figure(figsize=(10, 10))
		colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'gray', 'orange', 'purple']
		digits = range(10)
		for i, c in zip(digits, colors):
			plt.scatter(visualize_tsne[labels_tot==i, 0], visualize_tsne[labels_tot==i, 1], c=c, marker='+', label=i)
		plt.legend()
		plt.xticks([])
		plt.yticks([])
		plt.savefig(self.dir + '/epoch_{}_vis.png'.format(epoch))

		# k means clustering on latent representations
		kmeans = KMeans(n_clusters=10, n_init=10)
		kmeans.fit(latent_tot)
		y_pred = kmeans.predict(latent_tot)
		k_acc = cluster_acc(y_pred, labels_tot)
		print('epoch: {}, Kmeans acc: {}'.format(epoch, k_acc))

		# Gaussian Mixture clustering on latent representations
		gmm = GaussianMixture(n_components=10, n_init=10)
		gmm.fit(latent_tot)
		y_pred = gmm.predict(latent_tot)
		g_acc = cluster_acc(y_pred, labels_tot)
		print('epoch: {}, GMM acc: {}'.format(epoch, g_acc))

		# writing results to csv file
		logdict = dict(epoch=epoch, k_acc=k_acc, g_acc=g_acc)
		self.logwriter.writerow(logdict)


	@property
	def model_dir(self):
		return '{}_{}_{}'.format(self.model_name, self.dataset, self.z_dim)

	def make_dirs(self, dir):
		if not os.path.exists(dir):
			os.makedirs(dir)
		return dir

	def save_model(self, checkpoint_dir, step):
		dir = self.make_dirs(os.path.join(checkpoint_dir, self.model_dir))
		self.saver.save(self.sess, dir + '/ckpt', global_step = step)

	def load_model(self, checkpoint_dir):
		import re
		checkpoint_dir = os.path.join(checkpoint_dir, self.model_dir)
		ckpt = tf.train.get_checkpoint_state(checkpoint_dir)

		if ckpt and ckpt.model_checkpoint_path:
			ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
			self.saver.restore(self.sess, os.path.join(checkpoint_dir, ckpt_name))
			counter = int(next(re.finditer("(\d+)(?!.*\d)",ckpt_name)).group(0))
			print('Successful to find a checkpoint!')
			return counter
		else:
			print('Failed to find a checkpoint!')
			return 0
